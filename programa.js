var hombre = new Array("___\n", "   |\n", "   O\n", "  /", "|", "\\\n", "  /", " \\\n", "___")
var palabra
var libreriaPalabras = new Array("d i e n t e","h u m a n o", "a s t r o n a u t a", "s e r v i d o r", "p e r r o", " f u e g o ","n a d a r ", "n u d o", "m a r c o", "p a g i n a", "t e l a r a ñ a","d e s c a r g a r", "v i r t u a l", "m e m o r i a", "d i s c o", "l o c a l","c o n e c t a r", "d e s c o n e c t a r", "e l e f a n t e ", "i n t e r n e t", "d o m i n i o","d o n a r ", "h i p o p o t a m o", "e n l a c e", "p r o g r a m a r", "o r d e n a d o r", "l a p i z", "o f i c i n a", "i n f o r m e" )
var partes = 0
var colNueva = 0
var jugando


function ObtenerPalabra() {
   var indice = Math.round ( Math.random() * 27 )
   var cadena = new String( libreriaPalabras[indice] )
   palabra = cadena.split(" ")
}


function DibujarSujeto(visor, partes) {
   var dibujo = ""
   if (partes < 10)
      for(var x = 0; x < partes; x++) {
         dibujo += hombre[x]
      }
   visor.displayHombre.value = dibujo
}


function DibujarLetra(visor, letra) {
   var flag = false 
   var cadena = new String(visor.displayPalabra.value)
   var letrasCadena = cadena.split(" ")
   cadena = "" 
   for (var x = 0; x < palabra.length; x++) {
      if (palabra[x] == letra) {
         cadena += letra + " "
         flag = true
      } else
         cadena += letrasCadena[x] + " "
   }
   visor.displayPalabra.value = cadena
   return flag
}


function NuevaLetra(visor, letra) {
  
   visor.displayLetras.value += letra + " "
   if(colNueva == 3) {
      visor.displayLetras.value += "\n"
      colNueva = 0
   } else
      colNueva++
}


function Jugar(visor, letra) {
   if (jugando) {
      NuevaLetra(visor, letra)
      var acierto = DibujarLetra(visor, letra)
      if (!acierto)
         DibujarSujeto(visor, ++partes)
      if (partes == 9)
         FinJuego(false)
      else if (CompruebaPalabra(visor))
         FinJuego(true)
      } else {
         alert('Pulsa Juego nuevo para comenzar\nuna partida nueva.')
   }
}

function IniciarJuego(visor) {
   jugando = true
   partes = 0
   colNueva = 0
   ObtenerPalabra()
   DibujarSujeto(visor, partes)
   visor.displayPalabra.value = ""
   for (var x = 0; x < palabra.length; x++){
      visor.displayPalabra.value += "_ "
   visor.displayLetras.value = ""
   
}
}

function CompruebaPalabra(visor) {
   var fin = true
   var cadena = new String(visor.displayPalabra.value)
   var letrasCadena = cadena.split(" ")
   for(var x = 0; x < letrasCadena.length; x++)
      if (letrasCadena[x] == "_")
         fin = false
   return fin
}


function FinJuego(resultado) {
   var solucion = ""
   jugando = false 
   if (resultado) {
      document.visor.ganadas.value++
      alert("Ganaste  !")
   } else {
     document.visor.perdidas.value++
     for (var x = 0; x < palabra.length; x++)
        solucion += palabra[x]
     alert("Perdiste !\n La palabra era: " + solucion)
   }
}